import Vue from 'vue';
import Vuex from 'vuex';
import { stat } from 'fs';
import router from '../router';
Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        userSocket: null,
        userInfo: {},
        //The current conversation history
        convHistory: [],
        friendConnected: [],
        alert: []
    },
    mutations: {
        initInstance(state, token) {
            //if we getBack user data we can continue an make an connection with socket.io
            state.userSocket = io("http://localhost:3000/", {
                query: { token: localStorage.token },
                transports: ['websocket'],
                upgrade: false
            });

            state.userSocket.on("newConnection", data => {
                console.log('New connection');

                let found = false;
                //Solution for now 
                for (let i = state.friendConnected.length - 1; i >= Math.max(state.friendConnected.length - 5, 0); i--) {
                    if (state.friendConnected[i].id == data.user.id) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    state.friendConnected.push(data.user);
                }
            });

            state.userSocket.on("disconnection", data => {
                console.log('Disconnection');
                state.friendConnected.splice(state.friendConnected.indexOf(data.user));


                // state.alert.push({
                //     'type': 'Déconnexion',
                //     'extra': "Amis : " + data.user.username,
                // });
                // setTimeout(() => {
                //     state.alert.pop();
                // }, 10 * 1000);

            });

            state.userSocket.on("chat-message", data => {
                console.log(data);
                console.log("new message");

                console.log(router);

                let found = false;
                //Solution for now 
                for (let i = state.convHistory.length - 1; i >= Math.max(state.convHistory.length - 5, 0); i--) {
                    if (state.convHistory[i].messageID == data.messageID) {
                        found = true;
                        break;
                    }
                }


                // console.log(data);
                if (!found) {
                    state.convHistory.push(data);
                    
                    if (router.history.current.name != "conversation") {
                        state.alert.push({
                            'type': 'Nouveau message',
                            'extra': "Envoyée par " + data.exp.username,
                            'content': data.message,
                            'idExp' : data.exp.id
                        });
                    }



                    // setTimeout(() => {
                    //     state.alert.pop();
                    // }, 10 * 1000);

                }

            });
        },
        destroyInstance(state) {
            if (state.userSocket) {
                state.userSocket.disconnect();
                state.userSocket = null;

            }

            state.userInfo = {};
            state.convHistory = [];
            state.friendConnected = [];
        },
        loadConversationHistory(state, client) {
            console.log("load discution");
            //Make the request to get all the conversation
            //We join the room and we also make a get resquest to get the conversation history
            //When we load a conv he is considered as see

            const URL = "http://localhost:3000/conversation/t";

            const option = {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + localStorage.token
                },
                body: JSON.stringify({
                    friendId: client.id
                })
            };

            fetch(URL, option).then(response => {
                response.json().then(data => {
                    console.log(data);
                    state.convHistory = data.conversation;
                });
            });
        },

        loadFriendConnected(state) {
            const URL = "http://localhost:3000/conversation";
            const option = {
                method: "POST",
                headers: {
                    Authorization: "Bearer " + localStorage.token
                }
            };

            fetch(URL, option).then(response => {
                response.json().then(data => {
                    console.log(data);
                    //we get back an array of user
                    state.friendConnected = data.clientConnected;
                });
            });
        },
        sendMessage(state, data) {
            state.userSocket.emit("chat-message", {
                room: data.room,
                message: data.message
            });
            state.convHistory.push({
                message: data.message,
                exp: "0"
            });
        },
        deleteMessageNotification(state,client){
            console.log("delete");
            console.log(client);
            console.log(state.alert);

            state.alert = state.alert.filter( alert => alert.idExp != client.id);


        }



    },
    actions: {
        initInstanceAction(context, token) {
            context.commit('initInstance', token);
        },
        loadConversationHistoryAction(context, client) {
            context.commit('loadConversationHistory', client);
        },
        loadFriendConnectedAction(context) {
            context.commit('loadFriendConnected');
        },
        sendMessageAction(context, data) {
            context.commit('sendMessage', data);
        },
        destroyInstanceAction(context) {
            context.commit('destroyInstance');
        },
        deleteMessageNotificationAction(context,client){
            console.log("delete");
            context.commit('deleteMessageNotification',client);
        }

    }
});
