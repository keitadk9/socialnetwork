import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Signup from './views/Signup.vue';
import Signin from './views/Signin.vue';
import Conversation from './views/Conversation.vue';
import DashBoard from './views/DashBoard.vue'
import App from './App.vue'
import store from './store/index.js'


Vue.use(Router);


function disconnect(){
  store.dispatch('destroyInstanceAction');
}

function checkTokenValidity() {
  if (localStorage.token) {
    const URL = 'http://localhost:3000/';

    fetch(URL)
      .then((body) => {
        body.json()
          .then(data => {
            if (!data.user) {
              //if the token is expire we redirect to then
              delete localStorage.token;

            } 
          })
      }).catch((err) => {
       console.log(err);
      })
  }
}

function redirectDashboard(to, from, next) {
  if (localStorage.token) {
    //if the token is here we verify if he is not expire
    //checkTokenValidity();
   next('/dashboard');
  } else {
    next();
    disconnect();
  }
}

function redirectHomePage(to, from, next) {
  if (localStorage.token) {
    //if the token is here we verify if he is not expire
    //checkTokenValidity();

    next();
  } else {
    next('/');
    disconnect();
  }
}


export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      beforeEnter: (to, from, next) => {
        //If the token is present into localStorage we redirect to the dashboard
        redirectDashboard(to, from, next);
      }
    },
    {
      path: '/signup',
      name: 'signup',
      component: Signup,
      beforeEnter: (to, from, next) => {
        //If the token is present into localStorage we redirect to the dashboard
        redirectDashboard(to, from, next);
      }
    }, {
      path: '/signin',
      name: 'signin',
      component: Signin,
      beforeEnter: (to, from, next) => {
        //If the token is present into localStorage we redirect to the dashboard
        redirectDashboard(to, from, next);
      }

    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: DashBoard,
      beforeEnter: (to, from, next) => {
        redirectHomePage(to, from, next);
      }
    }, {
      path: '/conversation',
      name: 'conversation',
      component: Conversation,
      beforeEnter: (to, from, next) => {
        redirectHomePage(to, from, next);
      }
    }
  ]
});
