
const express = require('express');
const volleyball = require('volleyball');
const checkToken = require('./auth/middleware.js')
const cors = require('cors')
const socket_io = require('socket.io');

const auth = require('./auth/index.js');
const search = require('./search/index.js');
const leaf = require('./leaf/index.js');


var clientsConnected = new Map();


app = express();

//middleware
app.use(express.json({ limit: "1mb" }));
app.use(volleyball);
// app.use(cors({
//     origin: 'http://192.168.0.34:8080'
// }));
app.use(cors());
app.use('/auth', auth);

app.use(checkToken.checkTokenSetUser);

app.get('/', (request, response) => {
    response.json({
        message: "Saluuuuu!",
        user: request.user
    });
});


app.use(checkToken.isLogginIn);
app.use('/search', search);
app.use('/leaf',leaf);


server = app.listen(3000, () => {
    console.log("Server is running on port 3000.");
});


//Stream part ############################################################################################################
const db = require('./db/db.js');
const convTable = db.get('messagesHistory');



const io = socket_io(server);
io.use(checkToken.checkTokenInSocket);

io.sockets.on('connection', (socket) => {
    if (!socket.user) {
        socket.disconnect();
        return;
    }
    console.log('We have a new connection: ' + socket.id);
    clientsConnected.set(socket.user.id, socket);

    //Quand on se connecte on rejoin sa propre room
    socket.join(socket.user.id)
    console.log(clientsConnected.size);

    socket.broadcast.emit('newConnection', {
        user: {
            "username": socket.user.username,
            "id": socket.user.id
        },
        message: socket.user.username + " is connected !"

    });


    socket.on('disconnect', (data) => {
        console.log("We lost a connection");

        socket.broadcast.emit('disconnection', {
            user: {
                "username": socket.user.username,
                "id": socket.user.id
            },
            message: socket.user.username + " is connected !"
        });


        clientsConnected.delete(socket.user.id);
        console.log(clientsConnected.size);
    })

    socket.on('chat-message', (data) => {
        console.log(data);
        const friendSocket = clientsConnected.get(data.room);

        //before send we save the message in our database
        const userID = socket.user.id;
        // console.log("user-id "+ userID);
        // console.log("room "+ data.room);

        //The friend
        const userFriendID = data.room;
        let conversationID;

        if (userID > userFriendID) {
            conversationID = userID + '-' + userFriendID;
        } else {
            conversationID = userFriendID + '-' + userID;
        }

        // console.log(conversationID);

        convTable.findOne({ 'convID': conversationID })
            .then((tableData) => {
                // console.log(tableData);
                if (tableData) {
                    let newConversation = tableData.conversation;
                    const content = {
                        "messageID": conversationID + "/" + (new Date),
                        "message": data.message,
                        "exp": socket.user,
                        date: new Date(),
                        vue: false
                    }
                    newConversation.push(content);
                    convTable.update({ 'convID': conversationID }, { $set: { 'conversation': newConversation } });
                    friendSocket.emit("chat-message", content);
                } else {
                    console.log("ConvHistory undefined");
                }
            })







    })
});


//Renvoie la liste des client connecté en enlevant celui avec l'ID 'id'
function getUserConnectedFor(id) {
    userData = [];

    for (let [k, v] of clientsConnected) {
        const { user } = v;
        // console.log(user);
        if (user.id != id) {
            userData.push({
                "username": user.username,
                "id": user.id
            });
        }
    }

    console.log(userData);

    return userData

}

app.post('/conversation', (request, response) => {
    const idResquester = request.user.id;

    response.json({
        clientConnected: getUserConnectedFor(idResquester)
    });

})

app.post('/conversation/t', (request, response) => {

    //The guy who asked for the page
    const userID = request.user.id;

    //The friend
    const userFriendID = request.body.friendId;
    let conversationID;

    if (userID > userFriendID) {
        conversationID = userID + '-' + userFriendID;
    } else {
        conversationID = userFriendID + '-' + userID;
    }

    convTable.findOne({ convID: conversationID })
        .then(conv => {
            //if we already have a conversation between this two person we send back the history
            if (conv) {

                //Before send back we set all the conversation not viewed at  true
                convTable.update({ convID: conversationID }, {
                    $set: {
                        'conversation': conv.conversation.map(element => {
                            //console.log(element);

                            if (element.exp.id != userID) {
                                element.vue = true;
                            }
                            return element;
                        })
                    }
                });


                response.json({
                    "conversation": conv.conversation,
                    "convID": conv.convID,
                    "message": "Got history"
                });


            } else {
                convTable.insert({
                    "convID": conversationID,
                    conversation: []
                });

                response.json({
                    "conversation": [],
                    "convID": conversationID,
                    "message": "Got no history"
                });
            }
        })




});




app.use(function errorHandler(err, request, response, next) {
    request.status(res.statusCode || 500);
    console.log("Erreur send " + err.stack);
    response.json({
        message: err.message,
        statck: err.stack
    })
});