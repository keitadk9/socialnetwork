const express = require('express');
const db = require('../db/db.js');
require('dotenv').config();
const jwt = require('jsonwebtoken');


const router = new express.Router();

const usersTable = db.get('users');


function verifyTokenValidity(token) {
    return jwt.verify(token, process.env.TOKEN_SECRET, (err, tokenVerified) => {
        if (err) {
            console.log(err);
            return false;
        } else {
            //if token is not expired
            console.log(tokenVerified);
            return tokenVerified.iat < tokenVerified.exp;
        }
    });
}
router.post('/', (request, response) => {

    //if we have an authentificate user
    if (request.user) {
        const user = request.user;
        //console.log(user);

        const { search } = request.body;


        usersTable.find({ username: search })
            .then(data => {
                console.log(data);

                response.json({
                    message: "result"
                });

            })




    } else {
        console.log("We have an uncoming visitor");
        response.json({
            message: "no result"
        });
    }

})



module.exports = router;
