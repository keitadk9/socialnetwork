const express = require('express');
const db = require('../db/db.js');


const router = new express.Router();
const usersTable = db.get('users');


// insert into database the leaf (message) at user index
router.post('/', (request, response) => {

    usersTable.findOne({ '_id': request.user.id })
        .then((tableData) => {
            // console.log(tableData);
            if (tableData) {
                let leafHistory = tableData.leafs;
                let latestLeaf = leafHistory[`${(new Date).toDateString()}`] || [];

                const content = {
                    "leafID": request.user.id + "/" + (new Date),
                    "leaf": request.body.leaf,
                    "comment": [],
                    "likes": 0,
                    "exp" : {
                        "username" : request.user.username,
                        "id" : request.user.id
                    },
                    date: new Date()
                }
                //the latest are at the begining
                latestLeaf.unshift(content);
                leafHistory[`${(new Date).toDateString()}`] = latestLeaf;
                usersTable.update({ '_id': request.user.id }, { $set: { 'leafs': leafHistory } });
                console.log("leaf inserted");
                response.json({
                    message : "clear"
                })
            } else {
                console.log("User undefined");
            }
        });
});


function getLatestLeaf(friendList,callback) {

    let latestLeafs = []
    let nbSearch = 0;
    if(friendList.size == 0){
        callback([]);
    }

    for (let [k, v ] of friendList) {
       // console.log(v);

        usersTable.findOne({ '_id': v._id })
            .then((friend) => {

                if (friend) {
                    console.log("Start");

                    const dateNow = new Date();

                    const leafs = friend.leafs;
                    //console.log(leafs);

                    for (var date in leafs) {
                        const dateLeafTime = new Date(date);

                        // console.log(dateNow);
                        // console.log(dateLeafTime);
                        // console.log(Math.abs(dateNow - dateLeafTime));
                        // console.log( 86400000 * 3);

                        //We compare date and add the leafs list to the list that we are gonna return if the difference is less than 3 day
                        if(Math.abs(dateNow - dateLeafTime) < 86400000 * 3 ){
                            latestLeafs.unshift(...leafs[`${date}`]);
                        }else{
                            break;
                        }
                    }
                }

                nbSearch++;

                if(nbSearch == friendList.size){
                    callback(latestLeafs);
                }
            });

    }
}
router.get('/', (request, response) => {

    usersTable.findOne({ '_id': request.user.id })
        .then((tableData) => {
            // console.log(tableData);
            if (tableData) {
                //We iterate trows all the friends and get their latest leaf
                let friendList = new Map(tableData.friends);

                getLatestLeaf(friendList, (latestLeafs) =>{
                    console.log("LatestLeafs");
                    console.log(latestLeafs);

                    const dateNow = new Date();

                    const leafs = tableData.leafs;
                    //console.log(leafs);

                    for (var date in leafs) {
                        const dateLeafTime = new Date(date);

                        // console.log(dateNow);
                        // console.log(dateLeafTime);
                        // console.log(Math.abs(dateNow - dateLeafTime));
                        // console.log( 86400000 * 3);

                        //We compare date and add the leafs list to the list that we are gonna return if the difference is less than 3 day
                        if(Math.abs(dateNow - dateLeafTime) < 86400000 * 3 ){
                            latestLeafs.unshift(...leafs[`${date}`]);
                        }else{
                            break;
                        }
                    }


                    response.json({
                        latestLeafs
                    });
                });


            } else {
                console.log("User undefined");
            }
        });




});



module.exports = router;